import React, { useEffect, useState } from "react";
import Hero from "./Components/Hero";
import About from "./Components/About";
import Work from "./Components/Work";
import Nav from "./Nav/Nav";
import Contact from "./Components/Contact";
import Footer from "./Components/Footer";
import "./App.css";

function App() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <>
      <Nav onMenuOpen={setIsMenuOpen} isMenuOpen={isMenuOpen} />
      <Hero />
      <About />
      <Work />
      <Contact />
      <Footer />
    </>
  );
}

export default App;
