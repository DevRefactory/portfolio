import React from "react";
import classes from "./Hero.module.css";
import Typical from "react-typical";

const Hero = () => {
  return (
    <hero className={classes.hero}>
      <h4>Hi, my name is</h4>
      <h1>Angel Kolev.</h1>

      <h2>I'm a</h2>
      <p>
        I'm a software engineer specializing in building exceptional digital
        experiences. Currently, I'm focused on building accessible,
        human-centered products at Upstatement.
      </p>
      <button className={classes.btn}>Get In Touch</button>
    </hero>
  );
};

export default Hero;
