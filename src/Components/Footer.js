import React from "react";
import classes from "./Footer.module.css";
const Footer = () => {
  return (
    <footer>
      <ul>
        <li>
          <a href="">
            <span>
              <i class="fab fa-github-square fa-2x"></i>
            </span>
          </a>
        </li>
        <li>
          <a href="">
            <span>
              <i class="fab fa-instagram-square fa-2x"></i>
            </span>
          </a>
        </li>
        <li>
          <a href="">
            <span>
              <i class="fab fa-facebook-square fa-2x"></i>
            </span>
          </a>
        </li>
        <li>
          <a href="">
            <span>
              <i class="fab fa-linkedin fa-2x"></i>
            </span>
          </a>
        </li>
        <li>
          <a href="">
            <span>
              <i class="fab fa-dribbble-square fa-2x"></i>
            </span>
          </a>
        </li>
      </ul>
      <p>
        Designed & Built by <span> Angel Kolev</span>
      </p>

      <ul className={classes.stats}>
        <li>
          <span>
            <i class="far fa-star fa-lg"></i>
          </span>
          2,527
        </li>
        <li>
          <span>
            <i class="fas fa-code-branch fa-lg"></i>
          </span>
          1,123
        </li>
      </ul>
    </footer>
  );
};

export default Footer;
