import React from "react";

const Contact = () => {
  return (
    <section>
      <h2>
        <span>04.</span>Contact
      </h2>
      <p>
        Although I'm not currently looking for any new opportunities, my inbox
        is always open. Whether you have a question or just want to say hi, I'll
        try my best to get back to you!
      </p>
      <button>Say Hello</button>
    </section>
  );
};

export default Contact;
