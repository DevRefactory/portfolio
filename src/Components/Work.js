import React, { useState } from "react";
import { data } from "../Assets/data";
import Button from "../UI/Button";
import classes from "./Work.module.css";

const Work = () => {
  const [listOfCompanies, setListOfCompanies] = useState(data);
  const [index, setIndex] = useState(0);

  console.log(listOfCompanies);
  return (
    <section className={classes.work}>
      <h2>
        <span>02.</span> Work
      </h2>
      <div className={classes["btn-wrapper"]}>
        {listOfCompanies.map((company, index) => {
          return (
            <div key={index}>
              <Button
                onClick={() => {
                  setIndex(company.id);
                }}
              >
                <span>{company.name}</span>
              </Button>
            </div>
          );
        })}
      </div>
      <h3>{listOfCompanies[index].title}</h3>
      <h3>{listOfCompanies[index].date}</h3>
      <ul>
        {listOfCompanies[index].tasks.map((task, index) => {
          return <li key={index}>{task}</li>;
        })}
      </ul>
    </section>
  );
};

export default Work;
