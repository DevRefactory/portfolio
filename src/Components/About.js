import React from "react";
import classes from "./About.module.css";

const About = () => {
  return (
    <section className={classes.about}>
      <h2>
        <span>01.</span> About Me
      </h2>
      <div>
        <p>
          Hello! My name is Brittany and I enjoy creating things that live on
          the internet. My interest in web development started back in 2012 when
          I decided to try editing custom Tumblr themes — turns out hacking
          together a custom reblog button taught me a lot about HTML & CSS!
        </p>
        <p>
          Fast-forward to today, and I've had the privilege of working at
          <span> an advertising agency</span>,<span> a start-up</span>,
          <span> a huge corporation</span>, and{" "}
          <span>a student-led design studio</span>. My main focus these days is
          building accessible, inclusive products and digital experiences at{" "}
          <span>Upstatement</span> for a variety of clients.
        </p>
        <p>Here are a few technologies I've been working with recently:</p>
      </div>
      <div className={classes.skills}>
        <ul>
          <li>JavaScript (ES6+)</li>
          <li>Eleventy</li>
          <li>Node.js</li>
        </ul>
        <ul>
          <li>React</li>
          <li>Vue</li>
          <li>WordPress</li>
        </ul>
      </div>
      <div className={classes["img-container"]}>
        <img src="https://picsum.photos/200/300" alt="Angel Kolev" />
        <div className={classes["img-shadow"]}></div>
      </div>

      <div className="skills-container"></div>
    </section>
  );
};

export default About;
