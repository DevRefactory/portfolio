import React, { useState } from "react";
import classes from "./Nav.module.css";

const Nav = ({ onMenuOpen, isMenuOpen }) => {
  const linkToggle = () => {
    onMenuOpen(!isMenuOpen);
  };

  const menu = (
    <ul className={classes.pages}>
      <li>
        <span>01.</span> About
      </li>
      <li>
        <span>02.</span>Experience
      </li>
      <li>
        <span>03.</span>Work
      </li>
      <li>
        <span>04.</span>Contact
      </li>
      <li>
        <button className={classes.resume}>Resume</button>
      </li>
    </ul>
  );

  return (
    <nav className={classes["nav-container"]}>
      <div className={classes.nav}>
        <span className={classes.logo}>A</span>
        <button className={classes.btn} onClick={linkToggle}>
          {isMenuOpen ? (
            <i class="fas fa-window-close fa-2x"></i>
          ) : (
            <i class="fas fa-bars fa-2x"></i>
          )}
        </button>
      </div>
      {isMenuOpen && menu}
    </nav>
  );
};

export default Nav;
