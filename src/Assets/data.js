export const data = [
  {
    id: 0,
    name: "Upstatement",
    title: "Engineer",
    date: "May 2018 - Present",
    tasks: [
      "Write modern, performant, maintainable code for a diverse array of client and internal projects",
      "Work with a variety of different languages, platforms, frameworks, and content management systems such as JavaScript, TypeScript, Gatsby, React, Craft, WordPress, Prismic, and Netlify",
      "Communicate with multi-disciplinary teams of engineers, designers, producers, and clients on a daily basis",
    ],
  },
  {
    id: 1,
    name: "Scout",
    title: "Studio Developer",
    date: "January - April 2018",
    tasks: [
      "Worked with a team of three designers to build a marketing website and e-commerce platform for blistabloc, an ambitious startup originating from Northeastern",
      "Helped solidify a brand direction for blistabloc that spans both packaging and web Interfaced with clients on a weekly basis, providing technological expertise",
    ],
  },
  {
    id: 2,
    name: "Upstatement",
    title: "Engineer",
    date: "May 2018 - Present",
    tasks: [
      "Write modern, performant, maintainable code for a diverse array of client and internal projects",
      "Work with a variety of different languages, platforms, frameworks, and content management systems such as JavaScript, TypeScript, Gatsby, React, Craft, WordPress, Prismic, and Netlify",
      "Communicate with multi-disciplinary teams of engineers, designers, producers, and clients on a daily basis",
    ],
  },
  {
    id: 3,
    name: "Upstatement",
    title: "Engineer",
    date: "May 2018 - Present",
    tasks:
      "Write modern, performant, maintainable code for a diverse array of client and internal projects Work with a variety of different languages, platforms, frameworks, and content management systems such as JavaScript, TypeScript, Gatsby, React, Craft, WordPress, Prismic, and Netlify Communicate with multi-disciplinary teams of engineers, designers, producers, and clients on a daily basis",
  },
  {
    id: 4,
    name: "Upstatement",
    title: "Engineer",
    date: "May 2018 - Present",
    tasks:
      "Write modern, performant, maintainable code for a diverse array of client and internal projects Work with a variety of different languages, platforms, frameworks, and content management systems such as JavaScript, TypeScript, Gatsby, React, Craft, WordPress, Prismic, and Netlify Communicate with multi-disciplinary teams of engineers, designers, producers, and clients on a daily basis",
  },
  {
    id: 5,
    name: "Upstatement",
    title: "Engineer",
    date: "May 2018 - Present",
    tasks:
      "Write modern, performant, maintainable code for a diverse array of client and internal projects Work with a variety of different languages, platforms, frameworks, and content management systems such as JavaScript, TypeScript, Gatsby, React, Craft, WordPress, Prismic, and Netlify Communicate with multi-disciplinary teams of engineers, designers, producers, and clients on a daily basis",
  },
];
